package main

import (
	"log"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {
	app := app.NewWithID("Volume Indicator")
	w := app.NewWindow("Volume Indicator")

	progress := widget.NewProgressBar()
	value_shown := getValue()
	progress.Value = value_shown

	w.SetContent(container.NewVBox(progress))
	w.Resize(fyne.NewSize(300, 45))
	w.FixedSize()
	w.CenterOnScreen()
	w.SetMaster()

	go func() {
		time.Sleep(2 * time.Second)
		w.Close()
		app.Quit()
	}()

	w.ShowAndRun()
}

func getValue() float64 {
	brightness := GetBrightness()
	b_value := brightness / 100
	if b_value > 100 {
		return 100
	} else if b_value < 0 {
		return 0
	}
	return b_value
}

func GetBrightness() float64 {
	out, err := exec.Command("bash", "-c", "light").Output()
	if err != nil {
		log.Fatalf("exec.Command failed with %s\n", err)
	}
	v := strings.TrimSpace((string(out)))
	vFlt, _ := strconv.ParseFloat(v, 64)
	return vFlt
}
