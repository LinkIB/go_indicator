package main

import (
	"log"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {
	app := app.NewWithID("Volume Indicator")
	w := app.NewWindow("Volume Indicator")

	progress := widget.NewProgressBar()
	value_shown := getValue()
	progress.Value = value_shown

	w.SetContent(container.NewVBox(progress))
	w.Resize(fyne.NewSize(300, 45))
	w.FixedSize()
	w.CenterOnScreen()
	w.SetMaster()

	go func() {
		time.Sleep(2 * time.Second)
		w.Close()
		app.Quit()
	}()

	w.ShowAndRun()
}

func getValue() float64 {
	vol_value := GetVolume()
	vol := vol_value / 100
	if vol > 100 {
		return 100
	} else if vol < 0 {
		return 0
	}
	return vol
}

// get the volume using bash, pactl, grep, awk
func GetVolume() float64 {
	out, err := exec.Command("bash", "-c", "pactl list sinks | grep -m1 \"Volume:\" | awk '{print $5}'").Output()
	if err != nil {
		log.Fatalf("exec.Command failed with %s\n", err)
	}
	vPerc := strings.TrimSpace((string(out)))
	vStr := vPerc[:len(vPerc)-1]
	vFlt, _ := strconv.ParseFloat(vStr, 64)
	return vFlt
}
